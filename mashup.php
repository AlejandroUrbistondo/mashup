<?php 
function auth()
{
	$url_twitch = "https://id.twitch.tv/oauth2/token?client_id=17i7qdkm0f0v5duu1x0jb32oc95vpe&client_secret=9aanvitpikgw1o1x9mx6r3zr0rjkf2&grant_type=client_credentials";
	$curl = curl_init($url_twitch);
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, "true");
	$respuesta_curl = curl_exec($curl);
	curl_close($curl);
	$respuesta = json_decode($respuesta_curl);
	$token = $respuesta->access_token;
	return $token;
}

function buscarJuego($juego)
{
	$token = auth();
	$url_gamesdb = "https://api.igdb.com/v4/games";
	$ch = curl_init($url_gamesdb);
	$payload = 'search "'.$juego.'"; fields name, slug, url; limit 10;';
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
	curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Client-ID:17i7qdkm0f0v5duu1x0jb32oc95vpe',
												'Authorization: Bearer '.$token));
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec($ch);
	curl_close($ch);
	$respuesta_games = json_decode($result);

	//var_dump($respuesta_games);
	return $respuesta_games;
}

function buscarYoutube($nombreJuego)
{	
	$url_youtube = 
	"https://www.googleapis.com/youtube/v3/search?key=AIzaSyCLVOUussbnEzg0byg0aJHmq9rygBfXgKY&part=snippet&q=".$nombreJuego;
	$curl = curl_init($url_youtube);
	curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$respuesta_curl = curl_exec($curl);
	curl_close($curl);
	$respuesta = json_decode($respuesta_curl);

	return $respuesta;
}
 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Busqueda Videojuegos</title>
</head>
<body>
	<div id="buscador">
		<form method="post">				
			<input type="text" name="juego" id="juego">
			<input type="submit" name="buscar" id="buscar" value="Buscar">
			<br>
			<sub>* La busqueda diferencia entre mayusculas y minusculas</sub>
		</form>
	</div>	
	<div id="juegos">		
		<?php 
			if(isset($_POST["buscar"]))
			{
				$juegos = buscarJuego($_POST["juego"]);

				echo "<ul>";
				foreach ($juegos as $juego)
				{
					echo "<li>";
					echo "<form method='post'>";
					echo $juego->name;
					echo "<br>";
					echo "<input type='submit' name='youtube' id='youtube' value='Buscar en YouTube'>";
					echo "<input type='hidden' id='nombreJuego' name='nombreJuego' value='".$juego->slug."'>";
					echo "</form>";
					echo "</li>";
				}
				echo "</ul>";
			}	
		?>		
	</div>
	<div id="vids">
		<br>
		<?php 
			if(isset($_POST["youtube"]))
			{				
				$videos = buscarYoutube($_POST["nombreJuego"]);
				
				foreach ($videos->items as $video) 
				{
					if(isset($video->id->videoId))
					{
						//var_dump($video->id->videoId);
						
						echo "<div>";
						echo "<iframe type='text/html' width='720' height='405' src='https://www.youtube.com/embed/".$video->id->videoId."' frameborder='0' allowfullscreen></iframe>";
						echo "<p>".$video->snippet->description."</p>";
						echo "</div>";
						
					}					
				}
			}
		?>
	</div>
</body>
</html>